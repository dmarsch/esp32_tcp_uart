#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sat Sep  11 11:35 2020

@author: Derek Marsch
"""

from socket import *

serverPort = 12347 #arbitrary number over 1024

serverSocket = socket(AF_INET, SOCK_STREAM) #This is for TCP

serverSocket.bind(('', serverPort)) #bind unused port to the given port number

serverSocket.listen(1) #allow server to accept a connection

print("script started \n") #for testing

while 1: #infinite loop to wait for connection

    myConnectionSocket, addr = serverSocket.accept()  #accept the connection to transmit data at bound address
    
    print("socket accepted\n") #for testing
    
    content = myConnectionSocket.recv(1024)  #only receive up to 1024 bytes of data at a time
    
    print(f'content: {content}') #display the data from ESP32
    # if (CMD == "HELLO\n"):
    #     myConnectionSocket.send("How are you today?\n".encode())  #Server send bytes to Client
    # elif (CMD == "NAME\n"):
    #     myConnectionSocket.send("My name is Derek\n".encode())
    # elif (CMD == "BYE\n"):
    #     myConnectionSocket.send("Have a nice day!\n".encode())
    # else:
    #     myConnectionSocket.send("Invalid Input!\n".encode())
    
    myConnectionSocket.close()

